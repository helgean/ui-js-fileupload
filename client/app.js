import { template } from '../lib/ui-js-lib.js';
import '../lib/ui-js-fileupload.js';
import './file-list.js';
import './progressbar.js';

const tpl = template`
  <ui-js-fileupload url="/upload" parent-drop="true"></ui-js-fileupload>
  <file-list></file-list>
  <progress-bar></progress-bar>

  <button class="file-select-btn w3-button w3-circle w3-ripple w3-teal" aria-label="Add">
    <i class="material-icons">add</i>
  </button>
`;

class UploadApp extends HTMLElement {

  connectedCallback() {
    tpl().render(this);

    this.progressBar = this.querySelector('progress-bar');
    this.progressBar.progress = 0;
    const fileUpload = this.querySelector('ui-js-fileupload');
    const fileList = this.querySelector('file-list');

    this.querySelector('.file-select-btn').addEventListener("click", () =>{
      fileUpload.selectFiles();
    });

    fileUpload.addEventListener("start", (e) =>{
      this.filesInTransit = e.detail.files;
    });
    fileUpload.addEventListener("progress", (e) => {
      this.progressBar.progress = e.detail.percent;
    });
    fileUpload.addEventListener("uploaded", (e) => {
      this.filesInTransit = [];
      fileList.refresh(fileUpload.files);
    });
  }
}

customElements.define('upload-app', UploadApp);