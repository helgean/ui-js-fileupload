import { template } from '../lib/ui-js-lib.js';

const tpl = template`
  <div class="w3-light-grey">
    <div class="progress-bar w3-green" style="height:4px;width:0"></div>
  </div>
`;

class ProgressBar extends HTMLElement {
  constructor() {
    super();
  }

  static get observedAttributes() {
    return ['progress'];
  }

  attributeChangedCallback(attrName, oldValue, newValue) {
    if (newValue !== oldValue) {
      this.progress = parseInt(newValue);
    }
  }

  connectedCallback() {
    tpl().render(this);
    this.bar = this.querySelector('.progress-bar');
  }

  set progress(value) {
    this.bar.style.width = `${value}%`;
    this.setAttribute('progress', value);
  }

  get progress() {
    return parseInt(this.bar.style.width);
  }
}

customElements.define('progress-bar', ProgressBar);