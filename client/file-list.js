import { template } from '../lib/ui-js-lib.js';

const tpl = template`
  <ul class="w3-ul w3-hoverable"></ul>
`;

const itemtpl = template`
  <li class="list-item w3-bar w3-padding" tabindex="0">
    <span class="list-item-icon w3-bar-item w3-circle"><i class="material-icons w3-bar-item w3-circle">note_add</i></span>
    <span class="list-item-text w3-bar-item">${'fileName'}</span>
  </li>
`;

class FileList extends HTMLElement {

  constructor() {
    super();
  }

  connectedCallback() {
    this.refresh([]);

    /*this.querySelector('.file-delete-btn').addEventListener("click", () =>{

    });*/
  }

  refresh(files) {
    const list = tpl().clone().firstChild;

    for (const file of files)
      list.appendChild(itemtpl(file).clone());

    this.replaceChildren(list);
  }
}

customElements.define('file-list', FileList);